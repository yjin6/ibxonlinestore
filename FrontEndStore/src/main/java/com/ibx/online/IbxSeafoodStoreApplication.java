package com.ibx.online;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbxSeafoodStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(IbxSeafoodStoreApplication.class, args);
	}
}
