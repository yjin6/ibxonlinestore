package com.ibx.online.service;

import java.util.List;

import com.ibx.online.model.Product;

public interface ProductService {
	
	Product save(Product prod);

	List<Product> findAll();
	
	Product findOne(Long id);
	
	void removeOne(Long id);
}
