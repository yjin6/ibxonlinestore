package com.ibx.online.service;

import com.ibx.online.model.UserPayment;

public interface UserPaymentService {
	UserPayment findById(Long id);
	
	void removeById(Long id);
}
