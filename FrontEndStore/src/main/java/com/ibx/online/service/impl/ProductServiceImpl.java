package com.ibx.online.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibx.online.repository.ProductRepository;
import com.ibx.online.model.Product;
import com.ibx.online.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProductRepository productRepository;
	
	public Product save(Product prod) {
		return productRepository.save(prod);
	}
	
	public List<Product> findAll() {
		return (List<Product>) productRepository.findAll();
	}
	
	public Product findOne(Long id) {
		return productRepository.findOne(id);
	}
	
	public void removeOne(Long id) {
		productRepository.delete(id);
	}
}
