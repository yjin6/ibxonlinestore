package com.ibx.online.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibx.online.model.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByname(String name);
}
