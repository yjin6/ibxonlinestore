package com.ibx.online.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibx.online.model.UserPayment;

public interface UserPaymentRepository extends CrudRepository<UserPayment, Long>{

}
