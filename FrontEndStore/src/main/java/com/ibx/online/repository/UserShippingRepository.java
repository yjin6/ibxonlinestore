package com.ibx.online.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibx.online.model.UserShipping;

public interface UserShippingRepository extends CrudRepository<UserShipping, Long> {
	
	

}
