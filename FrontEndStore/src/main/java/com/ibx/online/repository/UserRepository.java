package com.ibx.online.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibx.online.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);
	
	User findByEmail(String email);
}
