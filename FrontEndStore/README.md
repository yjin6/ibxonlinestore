# README #

## IBX Seafood Online Ordering System
* Version 0.0.3
* Ongoing: Customer edit shipping/billing address
* Test username: hawk
* Test password: 123456

### Finished Components ###
* Customer register/login/logout (security/auto-confirm email)
* Customer view product list
* Customer update profile

### Remaining Components ###
* Customer edit shipping/billing address
* Customer edit payment method & credit card
* Customer order product
* Shopping cart
* Order placement

### Contribution guidelines ###

Full-stack Engineer: Ye Jin;
Functionality Tester: Ye Jin

### Who do I talk to? ###

Repo owner: Ye Jin;
Supervisor: Wei Jiang

Team contact: yjin6@ncsu.edu
