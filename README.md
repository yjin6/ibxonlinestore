# README #

## What is this repository for? ###

###IBX seafood retail store online ordering system
* Folder: FrontEndStore
* Version 0.0.3 
* ongoing: shopping cart model and customer's shipping/payment
###Admin Portal system for IBX staff
* Folder: BackEndAdminPortal
* Version 0.1.0
* ongoing: update product function

### How do I get set up? ###

##Runtime Prerequisites:
JRE 8; Apache Tomcat Server 8.0

###Development Environment:
MacOS/Linux; Eclipse/Intellij

###Development Dependencies (check pom.xml in each folder):
Including: Maven; Spring(web, context, security, etc.); Hibernate; MySQL; HTML; CSS; JavaScript; Bootstrap.

### Contribution guidelines ###

Full-stack Engineer: Ye Jin;
Functionality Tester: Ye Jin

### Who do I talk to? ###

Repo owner: Ye Jin; 
Supervisor: Wei Jiang

Team contact: yjin6@ncsu.edu
