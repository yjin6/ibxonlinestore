# README #

##Admin Portal system for IBX staff
* Version 0.1.0
* ongoing: update product function
* Test admin name: admin
* Test admin password: admin

### Finished Components ###
* Admin login/logout
* Show product List
* Add product
* Delete product

### Remaining Components ###
* Update product
* Add more admins users

### Contribution guidelines ###

Full-stack Engineer: Ye Jin;
Functionality Tester: Ye Jin

### Who do I talk to? ###

Repo owner: Ye Jin;
Supervisor: Wei Jiang

Team contact: yjin6@ncsu.edu
