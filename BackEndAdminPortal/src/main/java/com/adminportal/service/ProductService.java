package com.adminportal.service;

import java.util.List;

import com.adminportal.model.Product;

public interface ProductService {
	
	Product save(Product prod);

	List<Product> findAll();
	
	Product findOne(Long id);
	
	void removeOne(Long id);
}
