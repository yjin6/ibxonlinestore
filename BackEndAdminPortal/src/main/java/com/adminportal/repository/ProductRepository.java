package com.adminportal.repository;

import org.springframework.data.repository.CrudRepository;

import com.adminportal.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long>{

}
